<?php

class Menu extends AppModel {

	public $hasMany = [
		'MenuDetail' => [
			'className' => 'MenuDetail',
		],
		'Order' => [
			'className' => 'Order',
		],
	];
}